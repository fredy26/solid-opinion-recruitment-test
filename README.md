# README #

Recruitment testing project
Isomorphic hand-made

### What is this repository for? ###

For Solid Opinion Company
ver 1.0


### How do I get set up? ###

- Run ```yarn```
- Run ```yarn start``` in one terminal window and ```yarn dev:wds``` in an other terminal to initiate dev workaround
- Run ```yarn prod:build``` to build production code
- Run ``` yarn prod:start``` to fire up PM2 production server
