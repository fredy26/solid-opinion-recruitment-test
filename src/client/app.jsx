// @flow

import React from 'react'
import MainView from './Views/main-view'

const App = () => (
  <div>
    <MainView />
  </div>
)

export default App
