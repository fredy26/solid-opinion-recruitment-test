import Immutable from 'immutable'
import type { fromJS as Immut } from 'immutable'
// @flow
import { SAY_HELLO2MAIN, SAY_HELLO2IFRAME, FRAME_LOADED } from './actions'

const initialState = Immutable.fromJS({
  message: 'Hello world',
  iframeMessage: 'Hello iframe',
  loaded: false,
})

const mainReducer = (state: Immut = initialState, action: {type: string, payload: any}) => {
  switch (action.type) {
    case SAY_HELLO2MAIN:
      return state.set('message', action.payload)
    case SAY_HELLO2IFRAME:
      return state.set('iframeMessage', action.payload)
    case FRAME_LOADED:
      return state.set('loaded', true)
    default:
      return state
  }
}

export default mainReducer
