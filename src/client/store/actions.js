// @flow

import { createAction } from 'redux-actions'

export const SAY_HELLO2MAIN = 'SAY_HELLO2MAIN'
export const SAY_HELLO2IFRAME = 'SAY_HELLO2IFRAME'
export const FRAME_LOADED = 'FRAME_LOADED'

export const sayHello2Main = createAction(SAY_HELLO2MAIN)
export const sayHello2IFrame = createAction(SAY_HELLO2IFRAME)

export const confirmLoaded = createAction(FRAME_LOADED)
