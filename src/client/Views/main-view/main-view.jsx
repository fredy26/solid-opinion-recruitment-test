// @flow
/* eslint-disable class-methods-use-this, no-console */
import * as React from 'react'
import { connect } from 'react-redux'

import { sayHello2IFrame, sayHello2Main } from '@app-store/actions'
import View from './view'

type State = {
  mainInput: string,
  iframeInput: string
}

type Props = {
  dispatch: Function,
  message: string,
  iframeMessage: string
}

class MainView extends React.Component<Props, State> {

  constructor() {
    super()
    this.handleSendMessage2IFrame = this.handleSendMessage2IFrame.bind(this)
    this.handleSendMessage2Main = this.handleSendMessage2Main.bind(this)
    this.handleMainInputChange = this.handleMainInputChange.bind(this)
    this.handleIFrameInputChange = this.handleIFrameInputChange.bind(this)
  }

  state = {
    mainInput: 'Hello World',
    iframeInput: 'Hello IWorld',
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.message !== this.props.message) {
      console.info('New message for MainView')
      this.setState({ mainInput: nextProps.message })
    }
    if (nextProps.iframeMessage !== this.props.iframeMessage) {
      console.info('New message for IFrame')
      this.setState({ iframeInput: nextProps.iframeMessage })
    }
  }

  handleSendMessage2IFrame: Function
  handleSendMessage2Main: Function
  handleMainInputChange: Function
  handleIFrameInputChange: Function

  handleSendMessage2IFrame() {
    this.props.dispatch(sayHello2IFrame(this.state.mainInput))
  }

  handleSendMessage2Main() {
    this.props.dispatch(sayHello2Main(this.state.iframeInput))
  }

  handleMainInputChange(e) {
    this.setState({ mainInput: e.target.value })
  }

  handleIFrameInputChange(e) {
    this.setState({ iframeInput: e.target.value })
  }

  render() {
    return (
      <View
        {...this.state}
        handleMainClick={this.handleSendMessage2IFrame}
        handleIFrameClick={this.handleSendMessage2Main}
        onMainInputChange={this.handleMainInputChange}
        onIFrameInputChange={this.handleIFrameInputChange}
      />
    )
  }
}

const mapStateToProps = store => ({
  message: store.main.get('message'),
  iframeMessage: store.main.get('iframeMessage'),
})

export default connect(mapStateToProps)(MainView)
