// @flow

import React from 'react'
import { Button, Input, IFrame } from '@app-components'

export default (props: any) => (
  <span>
    <div>
      <Input onChange={props.onMainInputChange} value={props.mainInput} />
      <Button label="SEND" {...props} handleClick={props.handleMainClick} />
    </div>
    <IFrame>
      <h5>IFRAME</h5>
      <Input onChange={props.onIFrameInputChange} value={props.iframeInput} />
      <Button label="SEND" {...props} handleClick={props.handleIFrameClick} />
    </IFrame>
  </span>
)
