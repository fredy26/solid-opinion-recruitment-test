import React from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import blacklist from 'blacklist'
import { confirmLoaded } from '@app-store/actions'
import { connect } from 'react-redux'

class IFrame extends React.Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node,
    ]).isRequired,
    dispatch: PropTypes.func.isRequired,
  }

  componentDidMount() {
    setTimeout(this.renderFrame.bind(this), 0)
  }

  componentWillReceiveProps(nextProps) {
    const frame = ReactDOM.findDOMNode(this) // eslint-disable-line
    ReactDOM.render(
      nextProps.children,
      frame.contentDocument.getElementById('root'),
    )
  }

  shouldComponentUpdate() {
    return false
  }

  componentWillUnmount() {
    ReactDOM.unmountComponentAtNode(
      ReactDOM.findDOMNode(this).contentDocument.getElementById('root'),
    )
  }

  renderFrame() {
    const frame = ReactDOM.findDOMNode(this) // eslint-disable-line
    const root = document.createElement('div')

    root.setAttribute('id', 'root')

    this.head = frame.contentDocument.head
    this.body = frame.contentDocument.body
    this.body.appendChild(root)

    ReactDOM.render(this._children, root)
    this.props.dispatch(confirmLoaded())
  }

  render() {
    this._children = this.props.children
    // render children manually
    const props = blacklist(this.props, 'children', 'dispatch', 'iframeMessage', 'loaded')
    return <iframe frameBorder={0} {...props} title="IFRAME#1" onLoad={this.renderFrame} />
  }

}

const mapStateToProps = store => ({
  loaded: store.main.get('loaded'),
})

export default connect(mapStateToProps)(IFrame)
