import React from 'react'
import { StyledBtn } from '@styled-ui-kit'

// @flow

type Props = {
  label: string,
  handleClick: Function,
}

const Button = ({ label, handleClick }: Props) =>
  <StyledBtn onClick={handleClick}>{label}</StyledBtn>

export default Button
