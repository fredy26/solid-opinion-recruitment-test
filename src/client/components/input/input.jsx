import React from 'react'
import { StyledInput } from '@styled-ui-kit'

// @flow

type Props = {
  onChange: Function,
}

const Input = (props: Props) =>
  <StyledInput {...props} />

export default Input
